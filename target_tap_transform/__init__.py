#!/usr/bin/env python3

import argparse
import io
import os
import sys
import json
import csv
import threading
import http.client
import urllib
from datetime import datetime
import collections
import pkg_resources

from jsonschema.validators import Draft4Validator
import singer

import decimal

import data_mapper

SCHEMA_TYPE = ["schema_input", "schema_transform"]

class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return float(o)
        return super(DecimalEncoder, self).default(o)

logger = singer.get_logger()

def emit_state(state):
    if state is not None:
        line = json.dumps(state)
        logger.debug('Emitting state {}'.format(line))
        sys.stdout.write("{}\n".format(line))
        sys.stdout.flush()

def persist_messages(messages, schema_input, schema_transform):
    # target
    state = None
    schemas = {}
    key_properties = {}
    headers = {}
    validators = {}

    for message in messages:
        try:
            o = singer.parse_message(message).asdict()
        except json.decoder.JSONDecodeError:
            logger.error("Unable to parse:\n{}".format(message))
            raise
        message_type = o['type']
        if message_type == 'RECORD':
            stream = o['stream']
            if stream not in schemas:
                raise Exception("A record for stream {}"
                                "was encountered before a corresponding schema".format(stream))

            # record the input record
            record_transformed = None # default
            record = o['record']
            validators[stream].validate(record)

            # manipulate record
            try:
                # lets transform and validate record
                record_transformed = data_mapper.transform(record, schema_input["schema"], schema_transform)
            except data_mapper.TransformException as e:
                logger.warning("Record transformation failed. Got " + str(e)) # FIXME: what has failed
            except Exception as e:
                logger.warning("Record manipulation failed. Got " + str(e)) # FIXME: what has failed

            if record_transformed:
                # output stream
                try:
                    singer.write_records(stream, [record_transformed])
                except Exception as e:
                    logger.warning("Writing record failed. Got " + str(e)) 
            
        elif message_type == 'STATE':
            logger.debug('Setting state to {}'.format(o['value']))
            state = o['value']

        elif message_type == 'SCHEMA':
            stream = o['stream']
            schemas[stream] = o['schema'] # save tap schema
            validators[stream] = Draft4Validator(o['schema'])
            key_properties[stream] = o['key_properties']
            # write schema to output stream for next target
            singer.write_schema (stream, schemas[stream], key_properties[stream]) 
        else:
            logger.warning("Unknown message type {} in message {}".format(o['type'], o))

    return state

def schema_valid(schema):
    if type(schema) == dict:
        return "properties" in schema
    return False

def load_json(file_path):
    """
    Checks if file exists. If not we try to convert it to JSON.
    If convertion to JSON works we should raise Exception notifying that string must be path to file
    """
    json_content = None
    if os.path.exists(file_path):
        if os.path.isfile(file_path): 
            try:
                with open(file_path) as file_content:
                    try:
                        json_content = json.loads(file_content.read())
                    except Exception as e:
                        logger.error("Could not parse json file " + str(e))
            except IOError as e:
                logger.error("Could not load file " + str(e))
    return json_content

def load_schema(schema, schema_type):
    """
    Validates that schema is valid. Loading from file system if path to file

    Arguments:
        schema {object} -- an string or object of schema
        schema_type {string} -- either "schema_input" or "schema_transform"

    Returns:
        object -- returns the loaded schema
    """
    if not schema: 
        logger.error("Config file must have " + schema_type + " defined. Either a json object or a path to a file")
    
    if not schema_type in SCHEMA_TYPE:
        logger.error("Schema type should be one of " + str(SCHEMA_TYPE))

    # lets check if string is a path
    if type(schema) == str: # expect a path
        try:
            schema = load_json(schema)
        except Exception as e:
            logger.error("Could not load " + schema_type + " file due to " + str(e))
            return None
            
    # schema has value, lets check if it is valid
    if type(schema) == dict: # expect object
        if schema_valid(schema):
            return schema # all is fine, return schema
    else:
        logger.error("Config file must be either a json object or a path to a file")
    return None

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', help='Config file')
    args = parser.parse_args()

    if args.config:
        with open(args.config) as input_json:
            config = json.load(input_json)
    else:
        config = {}

    # load configuration
    schema_input = load_schema(config.get('schema_input', ''), "schema_input")
    if not schema_input: exit(1)
    
    schema_transform = load_schema(config.get('schema_transform', ''), "schema_transform")
    if not schema_transform: exit(1)

    # read input stream
    input_messages = io.TextIOWrapper(sys.stdin.buffer, encoding='utf-8')
    
    # start persistance
    state = persist_messages(input_messages,
                             schema_input,
                             schema_transform
                             )

    emit_state(state)
    logger.debug("Exiting normally")

if __name__ == '__main__':
    main()
