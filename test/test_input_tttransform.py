import sys,os,shutil
import copy
import_path = os.path.dirname(os.path.realpath(__file__)) + "/../"
print("Import path: " + import_path)
sys.path.append(import_path)

import unittest
import simplejson as json

# testing std-out
from io import StringIO
from contextlib import contextmanager

@contextmanager # https://schinckel.net/2013/04/15/capture-and-test-sys.stdout-sys.stderr-in-unittest.testcase/
def capture_stdout(command, *args, **kwargs):
    out, sys.stdout = sys.stdout, StringIO()
    try:
        command(*args, **kwargs)
        sys.stdout.seek(0)
        yield sys.stdout.read()
    finally:
        sys.stdout = out

from target_tap_transform import persist_messages, schema_valid, load_json, load_schema


class TestTargetTapTransform(unittest.TestCase):

    def getFirstFileContent(self):
        path = os.path.dirname(os.path.realpath(__file__)) + "/results/"
        for (dirpath, dirnames, filenames) in os.walk(path):
            if len(filenames) == 0: return ""
            with open(dirpath + filenames[0]) as f:
                return f.read()

    def setUp(self):
        # remove all test results
        path = os.path.dirname(os.path.realpath(__file__))
        shutil.rmtree(path + "/results/")
        os.mkdir(path + "/results/")
        self.target_schema_helloworld = copy.deepcopy(target_schema_helloworld)
        self.target_transform_helloworld = copy.deepcopy(target_transform_helloworld)

    def test_validate_schema_success(self):
        """Test should be able to load schema"""
        self.assertTrue( schema_valid(inventory_schema) )

    def test_validate_schema_failure(self):
        """Test should be able to load schema"""
        self.assertFalse( schema_valid("bad_schema") )
        self.assertFalse( schema_valid(32) )
        self.assertFalse( schema_valid(True) )
        self.assertFalse( schema_valid([]) )
        # change good schema to bad schema
        bad_schema = copy.deepcopy(inventory_schema)
        del bad_schema["properties"]
        self.assertFalse( schema_valid(bad_schema) )

    def test_load_schema_failure_nopath(self):
        """Tests that loading file from file system works"""
        self.assertEqual(load_schema("./test/schemaxxx/nosuch-schema.json", "schema_input"), None)

    def test_load_schema_failure_schematype(self):
        """Tests that loading file from file system works"""
        self.assertEqual(load_schema("./test/schema/nosuch-schema.json", "schema_NOTYPE"), None)

    def test_load_schema_failure_schematype(self):
        """Tests that loading file from file system works"""
        self.assertEqual(load_schema("./test/schema/bad-schema.json", "schema_input"), None)

    def test_load_schema_success_path(self):
        """Tests that loading file from file system works"""
        test_inventory_schema = load_schema("./test/schema/inventory-schema.json", "schema_input")
        self.assertDictEqual(inventory_schema, test_inventory_schema)

    def test_load_schema_success_object(self):
        """Tests that loading file from configworks"""
        test_inventory_schema = load_schema(inventory_schema, "schema_input")
        self.assertDictEqual(inventory_schema, test_inventory_schema)

    def test_load_schema_badschema(self):
        """Tests that loading file from configworks"""
        self.assertEqual(load_schema(bad_schema, "schema_input"), None) 

    def test_persist_messages_parse_helloworld(self):
        """Test running parser with required schema and input"""
        correct_out = """{"type": "SCHEMA", "stream": "inventory", "schema": {"type": "object", "properties": {"value": {"type": "string"}}}, "key_properties": []}
{"type": "RECORD", "stream": "inventory", "record": {"value": "worlds"}}
"""
        messages_stream = [target_schema_helloworld_str, target_record_helloworld_str]
        with capture_stdout(persist_messages, messages_stream, self.target_schema_helloworld, self.target_transform_helloworld) as output:
            self.assertEqual(correct_out, output)



inventory_schema = load_json("./test/schema/inventory-schema.json")
bad_schema = load_json("./test/schema/bad-schema.json")

target_inventory_schema = {
    "type":"SCHEMA", 
    "stream":"inventory",
    "key_properties":[],
    "schema": inventory_schema
}

target_inventory_record_door_1 = {
    "checked": False,
    "dimensions": {
        "width": 5,
        "height": 10
    },
    "id": 1,
    "name": "A green door",
    "price": 12.5,
    "tags": [
        "home",
        "green"
    ]
}

inventory_record_door_1 = {
    "type":"RECORD",
    "stream":"inventory",
    "schema":"inventory",
    "record": target_inventory_record_door_1
}

import decimal

class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return float(o)
        return super(DecimalEncoder, self).default(o)

target_schema_inventory_str = json.dumps(target_inventory_schema, use_decimal=True, cls=DecimalEncoder)
target_record_inventory_record_door_1_str = json.dumps(inventory_record_door_1, use_decimal=True, cls=DecimalEncoder)

############################################
#
#   Test data of hello world
#
############################################

target_schema_helloworld = {
    "type":"SCHEMA", 
    "stream":"inventory",
    "key_properties":[],
    "schema":{
        "type":"object", 
        "properties":{
            "value":{
                "type":"string"
            }
        }
    }
}
target_transform_helloworld = {
    "type":"object",
    "transform":{
        "type": "javascript",
        "code": "function _T(obj){ obj['value'] = obj['value'] + 's'; return obj; }"
    },
    "properties":{
        "value":{
            "type":"string"
        }
    }
}

target_record_helloworld = {
    "type":"RECORD",
    "stream":"inventory",
    "schema":"inventory",
    "record":{
        "value":"world"
    }
}
target_schema_helloworld_str = json.dumps(target_schema_helloworld)
target_record_helloworld_str = json.dumps(target_record_helloworld)


if __name__ == '__main__':
    unittest.main()