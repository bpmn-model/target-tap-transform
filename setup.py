#!/usr/bin/env python

from setuptools import setup

setup(name='target-tap-transform',
      version='0.1.0',
      description='Singer.io target that maps json schema to tap. Used for data transformation',
      author='Kjartan',
      url='https://singer.io',
      classifiers=['Programming Language :: Python :: 3 :: Only'],
      py_modules=['target_tap_transform'],
      install_requires=[
          'jsonschema==2.6.0',
          'singer-python==2.1.4',
      ],
      extras_require={
          'dev': [
              'ipdb==0.11'
          ]
      },      
      entry_points='''
          [console_scripts]
          target-tap-transform=target_tap_transform:main
      ''',
      packages=['target_tap_transform'],
      include_package_data=True,
)
